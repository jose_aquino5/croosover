
describe('VideoController', function() {
   beforeEach(module('app'));

    var $controller;


    beforeEach(inject(function($injector){
      // The $controller service is used to create instances of controllers
      $controller = $injector.get('$controller');;
    }));



    it('should be defined', function() {
     let $scope = {};
      var controller =  $controller('VideoController', {$scope: $scope });
      expect(controller).toBeDefined();
    });


    it('should return percent about a Array of int', function() {
      let $scope = {};
      var controller =  $controller('VideoController', {$scope: $scope });
      var arrayRating = [5,5,3,4]
      
      expect($scope.getRating(arrayRating)).toEqual(4);
    });

    it('should set $scope.isScreenXS to true', function() {
      let $scope = {};
      var controller =  $controller('VideoController', {$scope: $scope });
      $scope.killNiceScrollbar()
      expect($scope.isScreenXS).toEqual(true);
    });
 

});