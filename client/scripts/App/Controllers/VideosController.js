
app.controller('VideosController', function ($scope, $http) {
    $scope.model = { videos: [] }
    $scope.isExecutingLoadVideos = false;
    $scope.loadVideos = function () {
        $scope.isExecutingLoadVideos = true;
        $http.get("/videos?"+VMETA.querySessionId()+"&skip="+$scope.model.videos.length+"&limit=10")
            .then(function (response) {
                const data = response.data.data;

                if ($scope.model.videos.length > 0) {
                    $scope.model.videos = $scope.model.videos.concat(data);

                } else {
                    $scope.model.videos = data;
                }
                for (var index = 0; index < $scope.model.videos.length; index++) {
                    var element = $scope.model.videos[index].description;
                    $scope.model.videos[index].description = VMETA.truncateText(element, 110)
                }

                if(data.length < 1){
                    $scope.noMoreVideos = true;
                }
                $scope.isExecutingLoadVideos = false;
            });
    }



    $scope.ratingVideo = function(video, $event){
        $event.preventDefault();
     
        const rateGived = $($event.currentTarget.childNodes).index($event.target) + 1;
        $http.post("/video/ratings?"+VMETA.querySessionId(), {videoId: video._id, rating: rateGived}).then(function (response) {
                const data = response.data.data;
                if(response.data.status === "success"){
                    const rating = $scope.getRating(data.ratings);
                    VMETA.setRating($event.currentTarget, rating, rateGived);
                }else{
                    alertify.error('Error, try again', 'success', 5, function(){  console.log('dismissed'); });
                }
        });
          
    }

    $scope.buildStar = function () {
        for(let i = $scope.model.videos.length - 10; i < $scope.model.videos.length; i++){
            const rating = $scope.getRating($scope.model.videos[i].ratings);
            $("#"+$scope.model.videos[i]._id).starrr({
                readOnly: true,
                max: 5,
                rating: rating
            });
        }
    }

    $scope.getRating = function(ratings){
        let rating = 0;
        for(let x = 0; x < ratings.length; x++){
            rating += ratings[x];
        }
        return Math.round(rating/ratings.length);
    }
});