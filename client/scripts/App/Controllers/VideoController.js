
app.controller('VideoController', function ($scope, $http, $q, $routeParams, $location, screenSize) {
     $scope.model = { 
         videos: [],
         videoActual:  undefined
        }
    $scope.label = "Show more videos";
    $scope.isScreenXS = false;
    $scope.isExecutingLoadVideos = false;

    $scope.getVideoActual = function(video){
        if(video){
            $location.update_path("video/"+video._id, true);
            $scope.model.videoActual.description = VMETA.truncateText($scope.model.videoActual.description, 100)
            $scope.model.videos.push($scope.model.videoActual);
        }
        videoId = video ? video._id : $routeParams.id
        $scope.model.videoActual = undefined;
        $http.get("/video?"+VMETA.querySessionId()+"&videoId="+videoId).then(function(response){
            var deferred = $q.defer();

            if(response.data.status == "success"){
                 deferred.resolve(response.data.data);
            }else{
                 deferred.reject(response.data.data);
            }

            var promise =  deferred.promise;
              
            promise.then(function(data) {
                $scope.model.videoActual = data;
                $scope.removeCurrentVideo();
                if(!video){
                    $scope.loadVideos();
                }           
          }, function(reason) {
                alertify.error('Error, try again', 'success', 5, function(){  console.log('dismissed'); });
            });
        });  
    }
    $scope.getVideoActual();

    $scope.loadVideos = function (param) {
        $scope.isExecutingLoadVideos = true;
        const videosLegth = $scope.model.videos.length;
        $http.get("/videos?"+VMETA.querySessionId()+"&skip=0&limit="+($scope.model.videos.length+4))
        .then(function (response) {

            var deferred = $q.defer();

            if(response.data.status == "success"){
                 deferred.resolve(response.data.data);
            }else{
                 deferred.reject(response.data.data);
            }

            var promise =  deferred.promise;
              
            promise.then(function(data) {
                $scope.model.videos = data;
                $scope.removeCurrentVideo();

                for (var index = 0; index < $scope.model.videos.length; index++) {
                    var description = $scope.model.videos[index].description;
                    $scope.model.videos[index].description = VMETA.truncateText(description, 100)
                }

                if(data.length < 1){
                    $scope.label = "No more videos found";
                }
                $scope.isExecutingLoadVideos = false;

                }, function(reason) {
                        alertify.error('Error, try again', 'success', 5, function(){  console.log('dismissed'); });
                });

            }
        ); 
    }

    $scope.removeCurrentVideo = function(){
        for (var index = 0; index < $scope.model.videos.length; index++) {
            var element = $scope.model.videos[index];
            if (element._id == $scope.model.videoActual._id) {
                $scope.model.videos.splice(index, 1);
                break;
            }
        }
    }

    $scope.ratingVideo = function(video, $event){
        $event.preventDefault();
        const rateGived = $($event.currentTarget.childNodes).index($event.target) + 1;
        $http.post("/video/ratings?"+VMETA.querySessionId(), {videoId: video._id, rating: rateGived}).then(function (response) {
                const data = response.data.data;
                if(response.data.status === "success"){
                    const rating = $scope.getRating(data.ratings);
                    VMETA.setRating($event.currentTarget, rating, rateGived);;
                }else{
                    alertify.error('Error, try again', 'success', 5);
                }
        });
    }


    $scope.buildStar = function () {
        for(let i = 0; i < $scope.model.videos.length; i++){
            const rating = $scope.getRating($scope.model.videos[i].ratings);
            $("#"+$scope.model.videos[i]._id).starrr({
                readOnly: true,
                max: 5,
                rating: rating
            });
        }
    }

   $scope.buildStarActive = function(){
        const rating = $scope.getRating($scope.model.videoActual.ratings);
        $(".video-ranking-active").starrr({
            readOnly: true,
            max: 5,
            rating: rating
        });
   }

    $scope.getRating = function(ratings){
        let rating = 0;
        for(let x = 0; x < ratings.length; x++){
            rating += ratings[x];
        }
        return Math.round(rating/ratings.length);
    }

    $scope.killNiceScrollbar = function(){
        $(".my-scroll-area").removeClass("video-list").getNiceScroll().remove();
        $scope.isScreenXS = true;
    }

    screenSize.when('sm, md, lg', function() {
        if($scope.isScreenXS){
            $(".my-scroll-area").addClass("video-list").niceScroll({cursorcolor:"#32c8de"});
             $scope.isScreenXS = false;
        }
    });

    screenSize.when('xs', function() {
        if(!$scope.isScreenXS){
            $scope.killNiceScrollbar();
        }
    });

    if(screenSize.is('xs')){
        $(".my-scroll-area").removeClass("video-list")
    }else{
        $(".my-scroll-area").niceScroll({cursorcolor:"#32c8de"});
    }

});