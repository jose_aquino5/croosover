
app.controller('LoginController', function($scope, $http, $location, $q) {
    $scope.model = {username: null, password: null};

   $scope.access = function(isValid){
       if(isValid){
          $http.post("/user/auth", $scope.model).then(function(response){
            var deferred = $q.defer();

            if(response.data.status == "success"){
                 deferred.resolve(response.data);
            }else{
                 deferred.reject(response.data);
            }

            var promise =  deferred.promise;
              
            promise.then(function(data) {
                  VMETA.createCookie("sessionId", data.sessionId);
                  VMETA.createCookie("userName", data.username);
                  $location.path("/")              
          }, function(reason) {
                alertify.error('Email or password is incorrect, please try again', 'success', 5, function(){  console.log('dismissed'); });
            });
          }, function(response){ });
        }else{
            alertify.error('You attemp to break our security', 'success', 5, function(){  console.log('dismissed'); });

       }

   }
});