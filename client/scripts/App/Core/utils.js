var VMETA = VMETA || {};

VMETA.currentVideoPlaying;

VMETA.querySessionId = function () {
    return "sessionId=" + VMETA.getCookie();
};

VMETA.getCookie = function (cname) {
    var name = (cname ? cname : 'sessionId') + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

VMETA.killCookie = function (cname, expires) {
    document.cookie = cname + "=; expires=" + (expires ? expires : "Thu, 01 Jan 1970 00:00:00 UTC");
}

VMETA.createCookie = function (cname, value, expires) {
    document.cookie = cname + "=" + value + ";" + (expires ? "expires=" + expires : "");
}

VMETA.truncateText = function (descripcion, num) {
    if (descripcion.length > num) {
        descripcion = descripcion.substring(0, num);
        while (descripcion[descripcion.length - 1] != " ") {
            descripcion = descripcion.substring(0, descripcion.length - 1);
        }
        if (descripcion[descripcion.length - 1] == " ") {
            descripcion = descripcion.substring(0, descripcion.length - 1);
        }

        descripcion += "...";
    }
    return descripcion;
}

VMETA.setRating = function ($element, rating, rateGived) {
    var a = $($element).children('a');

    for (var index = 0; index < a.length; index++) {
        if (rating <= index) {
            $(a[index]).removeClass("fa-star");
            $(a[index]).addClass("fa-star-o");
        } else {
            $(a[index]).removeClass("fa-star-o");
            $(a[index]).addClass("fa-star");
        }
    }
    alertify.alert("<h3> <i class='fa fa-check text-succes'  aria-hidden='true'></i> Confirm</h3>", "<div class='text-center'>Successfully rated with " + rateGived + " stars</div>")
        .set({
            closable: false,
            label: "Ok"
        });
}
