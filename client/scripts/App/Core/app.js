
var app = angular.module("app", ["ngRoute", "infinite-scroll", "ngLocationUpdate", "matchMedia"]);



app.service('authInterceptor', function($q, $location) {
    var service = this;

    service.responseError = function(response) {
        if (response.status == 401){
            VMETA.killCookie("sessionId");

           $location.path("/login");
        }
        return $q.reject(response);
    };
})

app.config(function ($routeProvider, $httpProvider) {

    $httpProvider.interceptors.push('authInterceptor');

    $routeProvider
        .when("/login", {
            templateUrl: "views/login.html",
            controller: "LoginController"
        })
        .when("/", {
            templateUrl: "views/videos.html",
            controller: "VideosController"
        })
        .when("/video/:id", {
            templateUrl: "views/video.html",
             controller: "VideoController"
        })
         .when("/videos", {
              templateUrl: "views/videos.html",
            controller: "VideosController"
        })
        .otherwise({
            templateUrl: "views/errors/404-theme.html"
        });
});




app.run( function($rootScope, $location, $http) {

   $rootScope.isAuthenticated = false;
   $rootScope.search= "";
   $rootScope.logOut = function(){
       $http.get("/user/logout?"+VMETA.querySessionId()).then(function(response){
            if(response.data.status == "success"){
                  VMETA.killCookie("sessionId");
                  $location.path("/login");
            }
       });
   }
   
   $rootScope.$watch(function() { 
      return $location.path(); 
    },
    function(url){  
        $rootScope.showSearch = url;
        $rootScope.userName = VMETA.getCookie("userName");
       if(!VMETA.getCookie().trim() && url != "/login"){
            $rootScope.isAuthenticated = false;
            $location.path("/login");
       }

       if(VMETA.getCookie().trim() && url != "/login"){
            $rootScope.isAuthenticated = true;
       }

       if(VMETA.getCookie().trim() && url == "/login") {
             $rootScope.isAuthenticated = true;
            $location.path("/");
       }

       if(!VMETA.getCookie().trim() && url == "/login") {
             $rootScope.isAuthenticated = false;
       }
    });
});

app.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last) {
                $timeout(function() { 
                    scope.$eval(attr.onFinishRender);
                });
            }
        }
    }
});

app.directive('onPlayVideo', function ($timeout) {
    return {
        restrict: 'C',
        link: function (scope, element, attr) {
            element[0].onplaying = function() {
                if(VMETA.currentVideoPlaying && (VMETA.currentVideoPlaying != element[0])){
                    VMETA.currentVideoPlaying.pause();
                }
                VMETA.currentVideoPlaying = element[0];
            };
        }
    }
});
